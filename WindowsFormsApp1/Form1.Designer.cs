﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Emgu.CV;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private IContainer components = null;
        private PictureBox pictureBox1;
        private VideoCapture _capture;
        private Mat  _frame;

        private static object _locker = new object();

        public Form1()
        {
            InitializeComponent();

            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            // USB Webcam Stream Capture
            //_capture = new VideoCapture(0);

            // RTSP Server Stream Capture
            _capture = new VideoCapture(
                //@"rtsp://158.39.165.8:1935/rtplive/_definst_/hessdalen03.stream",
                @"rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov",
                VideoCapture.API.Ffmpeg
            );

            // to force UDP
            //setenv("OPENCV_FFMPEG_CAPTURE_OPTIONS", "rtsp_transport;udp");

            _capture.ImageGrabbed += ProcessFrame;

            _frame = new Mat();

            if (_capture != null)
            {
                try
                {
                    _capture.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ProcessFrame(object sender, EventArgs e)
        {
            if ((_capture != null)
                && (_capture.Ptr != IntPtr.Zero))
            {
                _capture.Retrieve(_frame, 0);

                lock (_locker)
                {
                    pictureBox1.Image = _frame.Bitmap;
                }
            }
        }

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1))
                .BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(37, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(707, 426);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1))
                .EndInit();
            this.ResumeLayout(false);
        }

        #endregion
    }
}